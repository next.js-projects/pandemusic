import { useState } from 'react'
import Paper from '@material-ui/core/Paper'
import InputBase from '@material-ui/core/InputBase'
import IconButton from '@material-ui/core/IconButton'
import {BsSearch as SearchIcon} from 'react-icons/bs'

import Style from './style'

export default function CustomizedInputBase() {

  const [inputValue, setInputValue] = useState("")

  function onClick() {
    if(inputValue.length >= 3) window.location.href = '/busca?f=' + inputValue
  }

  return (
    <Style>
      <Paper component="form" className="root">
        <InputBase
          placeholder="Buscar música"
          inputProps={{ 'aria-label': 'buscar musica' }}
          onChange={
            (e:React.ChangeEvent<HTMLInputElement>) => setInputValue(e.target.value)
          }
        />
        <IconButton 
          type="submit" 
          className="icon-button" 
          aria-label="search" 
          onClick={(e) => {e.preventDefault(); onClick();}}
        >
          <SearchIcon />
        </IconButton>
      </Paper>
    </Style>
  );
}