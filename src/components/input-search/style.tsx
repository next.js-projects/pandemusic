import styled, {StyledComponent, DefaultTheme } from "styled-components"
import { GlobalStyleProps } from '../../theme/globalStyle'

import myTheme from '../../theme'

const style: StyledComponent<any, GlobalStyleProps, DefaultTheme> = styled.div`

  width: 14rem;
  margin-right: 5px;

  form {
    padding: .2rem .4rem;
    display: flex;
    align-items: center;
    width: 14rem;

    background: ${props => myTheme.colors[props.theme.name].primary};
  }

  input {
    margin-left: .5rem;
    flex: 1;
    color: ${props => myTheme.colors[props.theme.name].background};
    font-weight: bold;
  }

  .icon-button: {
    padding: 1rem;
    color: ${props => myTheme.colors[props.theme.name].background};
  }

`
 export default style