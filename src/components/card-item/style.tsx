import styled, {StyledComponent, DefaultTheme } from "styled-components"
import { GlobalStyleProps } from '../../theme/globalStyle'

import myTheme from '../../theme'

export interface CardItemProps {
  backgroundImg: string;
}

const sytle: StyledComponent<any, GlobalStyleProps, DefaultTheme> = styled.article`

  position: relative;
  height: 15rem;
  width: 15rem;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: ${props => myTheme.colors[props.theme.name].primary};
  border-radius: 5px;

  box-shadow: 1px 1px 1px 0px ${props => myTheme.colors[props.theme.name].backgroundLink};

  z-index: 0;

  a {
    height: inherit;
    width: inherit;
    display: flex;
    align-items: center;
    
    z-index: 1;

    color: ${props => myTheme.colors[props.theme.name].secundary};

    .play-icon {
      opacity: 0;
      font-size: 15rem;
      padding: 1rem;
      position: absolute;
      transition: .5s;
    }
  }

  a:hover {
    .play-icon {  
      opacity: .8;
    }
  }

  .music-infos {
    background: ${props => myTheme.colors[props.theme.name].backgroundLink}db;
    padding: 1.6rem 0;
    width: 100%;

    p.music-name {
      font-weight: bold;
      margin-bottom: 1.6rem;
    }

    p.music-artist {
      font-size: 1.4rem;
    }
  }
  

  &&::after {
      content: '';
      
      top: 0;
      left: 0;
      bottom: 0;
      right: 0;
      position: absolute;
      opacity: 0.85;

      border-radius: 5px;

      z-index: -1;
  
      background-size: 150%;
      background-position-y: bottom;
      background-image: url(${(props: CardItemProps) => props.backgroundImg});


    }

`
 export default sytle