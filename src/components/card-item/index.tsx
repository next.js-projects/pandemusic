import Link from 'next/link'
import { FaRegPlayCircle } from 'react-icons/fa'

import Style from './style'

import { VideoProps } from '../../services/api/videos/_default'

export interface CardItemProps {
  item: VideoProps;
}

const cardItem: React.FC<CardItemProps> = ({item}) => {

  const thumbnailDefault = '/assets/images/thumbnails/thumbnail.jpeg'

  return (
    <Style backgroundImg={item.thumbnail || thumbnailDefault}>
      <Link href={item.videoUrl}>
        <a target="_blank">
          <div className="music-infos">
            <p className="music-name">{item.name}</p>
            <p className="music-artist">{item.artist}</p>
          </div>
          <FaRegPlayCircle className="play-icon" />
        </a>
      </Link>
    </Style>
  )
}

export default cardItem