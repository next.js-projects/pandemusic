import Style from './style'

export interface VideoPlayerProps {
  videoUrl?: string;
}

const testDefaultVideo = "https://r5---sn-bg0ezn7z.c.drive.google.com/videoplayback?expire=1597857547&ei=yyY9X4TsL9LczLUP0seVyAo&ip=2804:14c:71:283d:bca7:ebdf:2605:2427&cp=QVNOWUlfVlVSSFhOOjhCYkdtX1dEM3FydnM2Vm9OUmJYandXcEJuSUZmaXRUMmV6M0pqcVFBaXA&id=b3dd1fd6ffe8d5de&itag=18&source=webdrive&requiressl=yes&mh=Tp&mm=32&mn=sn-bg0ezn7z&ms=su&mv=m&mvi=5&pl=44&ttl=transient&susc=dr&driveid=1BHUH4Ol7m1-gaipFSAtJp3Grj_-9jWbe&app=explorer&mime=video/mp4&vprv=1&prv=1&dur=126.200&lmt=1588120741602728&mt=1597843103&sparams=expire,ei,ip,cp,id,itag,source,requiressl,ttl,susc,driveid,app,mime,vprv,prv,dur,lmt&sig=AOq0QJ8wRgIhAOH5GU83Yp2rSf1Vqzie4TGHfEFHjPxkJp5mGZs5pLK3AiEAnlsnSkb3mi60LfwxtTIRKVEa5TRdgQnq6GqTxb7FpQM=&lsparams=mh,mm,mn,ms,mv,mvi,pl&lsig=AG3C_xAwRgIhAIxtzCHjAkf3D1zxqihhpvaazkN7s5pjyzfhOZtkV59SAiEAoyhYIVwxFfmnIqWfB_C332oVAoBoD2XOfVG4QdduLIE=&cpn=v6Ju7zUm7BiJK6jw&c=WEB_EMBEDDED_PLAYER&cver=20200814"

const videoPlayer: React.FC<VideoPlayerProps> = ({videoUrl}) => {
  return (
    <Style 
      width="320" 
      height="240" 
      controls 
      src={videoUrl || testDefaultVideo}>
        Your browser does not support the video tag.
    </Style>
  )
}

export default videoPlayer