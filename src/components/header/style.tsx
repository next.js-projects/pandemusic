import styled, {StyledComponent, DefaultTheme } from "styled-components"
import { GlobalStyleProps } from '../../theme/globalStyle'

import myTheme from '../../theme'

const sytle: StyledComponent<any, GlobalStyleProps, DefaultTheme> = styled.header`
    position: fixed;
    left: 0;
    top: 0;
    height: var(--header-height);
    width: 100vw;
    z-index: 100;

    padding: .5rem 0;

    display: flex;
    align-items: center;

    box-shadow: 0px 2px 5px 0px ${props => myTheme.colors[props.theme.name].primary};

    background-color: ${props => myTheme.colors[props.theme.name].background};
    color: ${props => myTheme.colors[props.theme.name].secundary};

    img {
      padding-bottom: .2rem;
      height: calc(var(--header-height) - .5rem);
    }

    .title {
      font-size: 2rem;
      margin-left: 1rem;
    }

    a {
      display: flex;
      align-items: center;
    }

    .container {
      margin: auto;
      display: flex;
      justify-content: space-between;
      align-items: center;
      padding: 0 !important;
    }
`
 export default sytle