import Style from './style'

import InputSearch from '../input-search'

import Link from 'next/link'

export interface HeaderProps {
  headerClass?: string;
}

const header: React.FC<HeaderProps> = ({headerClass}) => {
  return (
    <Style className={`header ${headerClass}`}>
      <div className="container">
        <Link href="/">
          <a>
            <picture>
              <img src="/assets/images/brand/tittle.png" />
            </picture>
          </a>
        </Link>
        <div className="header-search"><InputSearch /></div>
      </div>
    </Style>
  )
}

export default header