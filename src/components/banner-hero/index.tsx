import Style from './style'
import { ReactChildren, CSSProperties } from 'react';

export interface BannerHeroProps {
  imageUrl: string;
  bannerClass?: string;
  bannerHeroStyle?: CSSProperties;
  overlayStyle?: CSSProperties;
  children?: ReactChildren;
}

const bannerHero: React.FC<BannerHeroProps> = ({imageUrl, bannerClass, bannerHeroStyle, overlayStyle, children}) => {
  return (
    <Style className={`banner-hero ${bannerClass}`} style={bannerHeroStyle} image={imageUrl}>
      <div className="overlay" style={overlayStyle} />
      {children}
    </Style>
  )
}

export default bannerHero