import styled, {StyledComponent, DefaultTheme } from "styled-components"
import { GlobalStyleProps } from '../../theme/globalStyle'

interface BannerHeroStyleProps {
  image: string
}

const sytle: StyledComponent<any, GlobalStyleProps, DefaultTheme> = styled.section`
    position: absolute;
    left: 0;
    top: 0;
    height: 100vh;
    width: 100vw;
    z-index: -1;

    background-size: cover;
    background-image: url(${(props: BannerHeroStyleProps) => props.image});
    
    .overlay {
      height: inherit;
      width: inherit;
    }
`
 export default sytle