// Context
import React from 'react'

// Components
import Head from '../../components/head'
import Header from '../../components/header'
import BannerHero from '../../components/banner-hero'

import PageSingleResponse from '../../components/page_single_response'

import HTMLParser from 'html-react-parser'

import Style from './style'

export interface PageDefaultProps {
  headTitle?: string;

  pageClass?: string;

  bannerHeroProps: any;

  pageTitle: string;
  pageDescrption?: string;

  api: any;

  functions: {
    Loading: Function;
    Content: Function;
  };

}

const defaultPage: React.FC<PageDefaultProps> = (props) => {  
  
  const mainContentComponent = (content: any) => {
    const functionType = !content ? 'Loading' : 'Content'

    return (
      <section className="main-content">
      { props.functions[functionType] && props.functions[functionType](content) }
      </section>
    )
  }

  return (
    <Style className={`container ${props.pageClass}`}>
      <Head title={props.headTitle} />
      <Header />
      <BannerHero {...props.bannerHeroProps} />
      <main>
        <h3><span className="title title-background">{props.pageTitle}</span></h3>
        <div className="page-description"><h5>{
          props.pageDescrption && HTMLParser(props.pageDescrption)
        }</h5></div>

        <PageSingleResponse 
          api={props.api}
          functions={{
            Content: (content: any) => mainContentComponent(content),
            Loading: () => mainContentComponent(null),
          }}
        />  
      </main>
    </Style>
  )
}

export default defaultPage