import styled from 'styled-components'

import myTheme from '../../theme'

export default styled.div`

  .main-banner-hero {
    top: var(--header-height);
    height: 35vh;
    background-position: center;
    background-attachment: fixed;
  }

  main {
    position: relative;
    top: 35vh;
    padding: 0 2rem;

    h3, .title {
      margin-bottom: 3rem;
    }
  
    .page-description {
      position: relative;
      padding: 6rem 1rem 1rem 1rem;
      top: -6rem;
      border-radius: 0 0 5px 5px;
      z-index: -10;

      background-color: ${props => myTheme.colors[props.theme.name].backgroundLink}d6;
    }

    h5 {
      font-size: 2rem;
      margin-bottom: 3rem;
    }
  }

  .main-content {
    background-color: ${props => myTheme.colors[props.theme.name].backgroundLink}26;
    margin-top: -5rem;
    margin-bottom: 2rem;
    h4 {
      font-size: 2.5rem;
      text-align: left;
      margin-top: 3rem;
      margin-bottom: 0 !important;

      background: ${props => myTheme.colors[props.theme.name].backgroundLink}d6;
      padding: 1rem;
      border-radius: 5px 5px 0 0;
    }
  }

  &&.album {
    .title {
      text-align: center;
    }

    ul {
      flex-wrap: wrap;
      justify-content: center;
      overflow-x: hidden;
      padding: 0 !important;
    }

    li {
      padding: .5rem !important;
    }

    .main-banner-hero {
      background-position-y: 40rem;
      background-position-x: center;
    }
  }

  @media (min-width: 500px) {
    .main-banner-hero {
      height: 70vh;
    }

    &&.album .main-banner-hero {
      background-position-y: -12rem;
    }

    main {
      top: 70vh;
    }
  }

  @media (min-width: 1200px) {
    .main-banner-hero {
      height: 80vh;
    }

    &&.album .main-banner-hero {
      background-position-y: -20rem;
    }

    main {
      top: 80vh;
    }
  }

`