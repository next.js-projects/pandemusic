import styled, {StyledComponent, DefaultTheme } from "styled-components"
import { GlobalStyleProps } from '../../theme/globalStyle'

import myTheme from '../../theme'

const sytle: StyledComponent<any, GlobalStyleProps, DefaultTheme> = styled.ul`

  display: flex;
  overflow-x: scroll;

  ::-webkit-scrollbar {
    height: 1rem;
  }

  /* Track */
  ::-webkit-scrollbar-track {
    box-shadow: inset 0 0 5px ${myTheme.colors.matterhorn}; 
    background-color: ${props => myTheme.colors[props.theme.name].backgroundLink};
    border-radius: 5px;
  }
  
  /* Handle */
  ::-webkit-scrollbar-thumb {
    background-color: ${myTheme.colors.matterhorn}; 
    border-radius: 5px;
  }

  /* Handle on hover */
  ::-webkit-scrollbar-thumb:hover {
    background-color: ${props => myTheme.colors[props.theme.name].background}; 
  }

  padding-left: 1rem;
  background-color: ${props => myTheme.colors[props.theme.name].backgroundLink}70;
  border-radius: 0 0 5px 5px;

  li {
    padding: 1rem;
  }

`
 export default sytle