import CardItem from '../card-item'
import Style from './style'

import { VideoProps } from '../../services/api/videos/_default'

export interface CardListProps {
  items: Array<VideoProps>
}

const cardList: React.FC<CardListProps> = ({items}) => {
  return (
    <Style>
    { 
      items.map((item: VideoProps) => 
      <li key={item.linkUrl}><CardItem item={item}/></li>
    )}
    </Style>
  )
}

export default cardList