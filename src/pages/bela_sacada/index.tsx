// API
import api_videos from '../../services/api/videos'

// Components
import DefaultPage from '../../components/page-default'
import CardList from '../../components/card-list'

import Style from './style'

const belaSacada = () => {

  const mainContentComponent = (content: any) => {

    // TODOs:
    // Quando estiver carregando, mostrar os cards vazios, ou com efeito de loading
    if(!content) return

    return (
      <>
        {/* <h4 className="title">Em destaque</h4>
        <CardList items={content.recents} /> */}

        <h4 className="title">Bela Sacada</h4>
        <CardList items={content} />     
      </>
    )
  }

  return (
    <Style className="app">
      <DefaultPage 
        headTitle='Pandemusic - Álbum: Bela "Sacada"'
        pageClass='album'
        bannerHeroProps={{
          imageUrl: "assets/images/banners/banner_guitar_2.jpg",
          bannerClass:"main-banner-hero"
        }}
  
        pageTitle='Álbum'
        pageDescrption='muito mais que uma boa ideia, essa é uma Bela "Sacada"'
  
        api={api_videos.bela_sacada}
  
        functions={{
          Content: (content: any) => mainContentComponent(content),
          Loading: () => mainContentComponent(null),
        }}
      />
    </Style>
  )
}

export default belaSacada