import { useEffect, useState } from 'react'
import { useRouter } from 'next/router'

// API
import api_videos from '../../services/api/videos'
import { VideoProps } from '../../services/api/videos/_default'

// Components
import DefaultPage from '../../components/page-default'
import CardList from '../../components/card-list'

import Style from './style'

const busca = () => {

  const route = useRouter()
  const [results, setResults] = useState([])
  
  useEffect(() => {
    const f = route.query.f as string
    let searchResults:any = []
    if(f) {
      ['emocoes', 'bela_sacada'].forEach((key: string) => {
        const album = api_videos[key]

        const albumResults = album.filter((video: VideoProps) => {
          const videoContent = [video.name, video.artist].join(' ').toLowerCase()
          return !!videoContent.match(f.toLowerCase())
        })
        searchResults = searchResults.concat(albumResults)
      })
      setResults(searchResults)
    }
  }, [route])

  const mainContentComponent = (content: any) => {

    if(!content) return

    return (
      <>
        <h4 className="title">{content.lenght ? 'Resultado da busca:': 'Nenhum resultado encontrado.'}</h4>
        <CardList items={content} />     
      </>
    )
  }

  const description = route.query.f ? 
  `Resultados para a busca: <strong>${route.query.f}</strong>`
  :
  'Por favor, escreva algo no campo de busca...'

  return (
    <Style className="app">
      <DefaultPage 
        headTitle='Pandemusic - Busca'
        pageClass='album'
        bannerHeroProps={{
          imageUrl: "assets/images/banners/banner_guitar_2.jpg",
          bannerClass:"main-banner-hero"
        }}
  
        pageTitle='Busca'
        pageDescrption={description}
  
        api={results}
  
        functions={{
          Content: (content: any) => mainContentComponent(content),
          Loading: () => mainContentComponent(null),
        }}
      />
    </Style>
  )
}

export default busca