import styled from 'styled-components'

export default styled.div`

  h5 strong {
    font-size: 1.5em;
    text-decoration: underline;
  }

`