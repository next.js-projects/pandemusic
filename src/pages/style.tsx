import styled from 'styled-components'

export default styled.div`

  .main-banner-hero {
    top: var(--header-height);
    height: 35vh;
    background-position: center;
    background-attachment: fixed;
  }

  main {
    position: relative;
    top: 35vh;
    padding: 0 2rem;

    h3, .title {
      margin-bottom: 3rem;
    }
  
    h5 {
      font-size: 2rem;
      margin-bottom: 3rem;
    }
  }

  .title a span {
    float: right;
  }

  @media (min-width: 500px) {
    .main-banner-hero {
      height: 70vh;
    }

    main {
      top: 70vh;
    }
  }

  @media (min-width: 1200px) {
    .main-banner-hero {
      height: 80vh;
    }

    main {
      top: 80vh;
    }
  }

`