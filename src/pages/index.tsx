// API
import api_videos from '../services/api/videos'

// Components
import DefaultPage from '../components/page-default'
import Link from 'next/link'
import CardList from '../components/card-list'

import Style from './style'

const home = () => {

  const mainContentComponent = (content: any) => {
    // TODOs:
    // Quando estiver carregando, mostrar os cards vazios, ou com efeito de loading
    if(!content) return

    return (
      <>
        <h4 className="title">Últimas Postagens</h4>
        <CardList items={content.recents} />

        <h4 className="title">As melhores</h4>
        <CardList items={content.tops} />

        <h4 className="title">
          <Link href="/emocoes_em_alto_lar"><a>Emoções em Alto Lar <span>+</span></a></Link>
        </h4>
        <CardList items={content.emocoes.slice(0,5)} />

        <h4 className="title">
          <Link href="/bela_sacada"><a>Bela "Sacada" <span>+</span></a></Link>
        </h4>
        <CardList items={content.bela_sacada.slice(0,5)} />     
      </>
    )
  }

  return (
    <Style className="app">
      <DefaultPage 
        headTitle='Pandemusic'
        pageClass=''
        bannerHeroProps={{
          imageUrl: "assets/images/banners/banner_guitar.jpg",
          bannerClass:"main-banner-hero"
        }}

        pageTitle='Pandemusic'
        pageDescrption='Verifique sua conexão, passe álcool em gel nas mãos, mantenha-se um metro de distância de qualquer pessoa e "bora" começar o show!'

        api={api_videos}

        functions={{
          Content: (content: any) => mainContentComponent(content),
          Loading: () => mainContentComponent(null),
        }}
      />
    </Style>
  )
}

export default home