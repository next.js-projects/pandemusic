import React, { ComponentType, PropsWithChildren} from 'react'

import ThemeContextProvider from '../services/contexts/theme'
import Layout from '../components/Layout'

export interface AppProps {
  Component: ComponentType;
  pageProps: PropsWithChildren<ComponentType>;
}

const App: React.FC<AppProps> = ({ Component, pageProps }) => {
  return (
    <ThemeContextProvider>
      <Layout><Component {...pageProps} /></Layout>
    </ThemeContextProvider>
  )
}

export default App