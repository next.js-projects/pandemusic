import { createGlobalStyle, GlobalStyleComponent, DefaultTheme } from "styled-components"
import myTheme from './index'

export interface GlobalStyleProps {
  theme: {
    name: string;
  }
}

const globalStyle: GlobalStyleComponent<GlobalStyleProps, DefaultTheme> = createGlobalStyle`

  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }
  
  html, body, #root {
    height: 100vh;
  }

  body {    
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;

    background-color: ${props => myTheme.colors[props.theme.name].background} !important;

    overflow-x: hidden;

    display: flex;
    justify-content: center;
    justify-items: center;
    text-align: center;
  }

  .app:before {
    content: '';
    position: fixed;
    height: 100%;
    width: 100vw;
    top: 0;
    left: 0;
    background-image: url(/assets/images/bg_notes.jpg);
    z-index: -100;
    opacity: 0.05;
    background-repeat: round;
  }

  .container {
    max-width: 1200px !important;
    padding: 1rem;
    width: 100vw;
  }

  ul { list-style: none; }

  img, picture, video, embed {
    max-width: 100%;
  }

  .banner-hero {
    box-shadow: inset ${props => myTheme.colors[props.theme.name].backgroundLink}80 0px 0px 10px 10px;
    border-bottom: 1px solid ${props => myTheme.colors[props.theme.name].backgroundLink}9e;
  }

  :root {
    --header-height: 6.5rem;
  }
`

export default globalStyle