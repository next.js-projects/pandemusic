import { GlobalStyleProps } from '../globalStyle'
import { GlobalStyleComponent, DefaultTheme } from "styled-components"

import buttons from './buttons'
import texts from './texts'
import hiddens from './hiddens'

const classes: Array<GlobalStyleComponent<GlobalStyleProps, DefaultTheme>> = [
  buttons,
  texts,
  hiddens,
]

export default classes