import { createGlobalStyle } from "styled-components";
import fontsConfig, { FontsProps } from "./fontsConfig";

let names: any = {}
const createFontNames = (font: FontsProps) => names[font.name.toLowerCase()] = font.name

let types: any = {}
const createType = (font: FontsProps) => {if(font.type) types[font.type] = font.name }


fontsConfig.forEach( (font: FontsProps) => {
  [
    createFontNames,
    createType,
  ].forEach( cFunction => cFunction(font))
})

const faces = 
  createGlobalStyle`
  ${fontsConfig
  .map(
    font => 
    `@font-face 
      { 
        font-family: ${font.name};
        ${font.url.map(url => `src: (${url});`).join("")}
      }
    `
  )
  .join(" ")}`

export default {
  names,
  types,
  Faces: faces,
}