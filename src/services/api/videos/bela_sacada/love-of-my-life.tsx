import videoPattern from '../_default'

export default videoPattern({
  name: 'Love of My Life',
  artist: 'Queen',
  videoUrl: 'https://drive.google.com/file/d/1AreyDPJSD3s4BQdKJKFS4eMLRvlmNFq3/view?usp=sharing',
})