import videoPattern from '../_default'

export default videoPattern({
  name: 'Stairway to Heaven',
  artist: 'Led Zeppelin',
  videoUrl: 'https://drive.google.com/file/d/1AgRrXmnpIsiyTXDkq5KCbCs69NCPX_h6/view?usp=sharing',
})