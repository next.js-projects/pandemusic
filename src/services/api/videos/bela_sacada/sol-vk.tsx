import videoPattern from '../_default'

export default videoPattern({
  name: 'Sol',
  artist: 'Victor Klein',
  linkUrl: 'sol-victor-klein',
  videoUrl: 'https://drive.google.com/file/d/1BHUH4Ol7m1-gaipFSAtJp3Grj_-9jWbe/view?usp=sharing',
})