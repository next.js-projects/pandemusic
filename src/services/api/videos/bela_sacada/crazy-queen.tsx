import videoPattern from '../_default'

export default videoPattern({
  name: 'Crazy Little Thing Called Love',
  artist: 'Queen',
  linkUrl: 'crazy-little-thing-called-love-queen',
  videoUrl: 'https://drive.google.com/file/d/1BQuW7OfwvW7-NrsP22X_vthaG4odUExA/view?usp=sharing',
})