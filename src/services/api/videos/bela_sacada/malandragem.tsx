import videoPattern from '../_default'

export default videoPattern({
  name: 'Malandragem',
  artist: 'Cássia Eller',
  linkUrl: 'malandragem-cassia-eller',
  videoUrl: 'https://drive.google.com/file/d/1CVpuv-jRM8aa7yPDsrJqjYEl_Ug4Bzip/view?usp=sharing',
})