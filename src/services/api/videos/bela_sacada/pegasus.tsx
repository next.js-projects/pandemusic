import videoPattern from '../_default'

export default videoPattern({
  name: 'Pegasus Fantasy',
  artist: 'CDZ',
  videoUrl: 'https://drive.google.com/file/d/1BTiOBktuIz7AD0ApHdpkF60QlcLn0_w6/view?usp=sharing',
})