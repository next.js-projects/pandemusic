import videoPattern from '../_default'

export default videoPattern({
  name: 'Time Like These',
  artist: 'Foo Fighters',
  linkUrl: 'time-like-these-foo-fighters',
  videoUrl: 'https://drive.google.com/file/d/1BUhN7qw95uzkieHruJ23PINf53ccWYpA/view?usp=sharing',
})