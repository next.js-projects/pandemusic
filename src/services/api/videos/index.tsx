import sol_vk from './bela_sacada/sol-vk'
import malandragem from './bela_sacada/malandragem'
import time_like_these from './bela_sacada/time-like-these'
import crazy from './bela_sacada/crazy-queen'
import dias_melhores from './bela_sacada/dias-melhores'
import love_of_my_life from './bela_sacada/love-of-my-life'
import pegasus from './bela_sacada/pegasus'
import stairway from './bela_sacada/stairway'
// come as you are
// será
// smells
// sol - j quest
// wasting love

import hey_jude from './emocoes/hey-jude'
import abraco from './emocoes/abraco'
import tear_in_heaven from './emocoes/tears-in-heaven'
import have_ever from './emocoes/have-ever'
import nothing_else from './emocoes/nothing-else'
import sinonimos from './emocoes/sinonimos'
import wind_of_change from './emocoes/wind-of-change'
// a tal canção
// behind blue eyes
// one
// quando sol
// sou dela
// wish you

import { VideoProps } from './_default'

export interface VideosAPI {
  tops: Array<VideoProps>;
  recents: Array<VideoProps>;
  emocoes: Array<VideoProps>;
  bela_sacada: Array<VideoProps>;
  [key:string]: any;
} 

export default {
  tops: [
    tear_in_heaven,
    sol_vk,
    hey_jude,
  ],
  
  recents: [
    tear_in_heaven,
    hey_jude,
    wind_of_change,
  ],

  emocoes: [
    tear_in_heaven,
    hey_jude,
    wind_of_change,
    have_ever,
    sinonimos,
    nothing_else,
    abraco,
  ],

  bela_sacada: [
    crazy,
    stairway,
    sol_vk,
    love_of_my_life,
    malandragem,
    pegasus,
    time_like_these,
    dias_melhores,

  ],
} as VideosAPI