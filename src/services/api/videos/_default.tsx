export interface VideoProps {
  name: string;
  artist?: string;
  linkUrl?: string;
  videoUrl: string;
  thumbnail?: string;
  tag?: Array<string>;
  hightlight?: boolean;
}

export default (props: VideoProps) => {

  const separator = "-"

  return {
    linkUrl: props.name.concat(separator, props.artist || "")
      .toLowerCase()
      .replace(/\s/g, separator),
    ...props,   
  }
}