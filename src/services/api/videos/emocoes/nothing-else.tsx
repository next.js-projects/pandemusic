import videoPattern from '../_default'

export default videoPattern({
  name: 'Nothing Else Matters',
  artist: 'Metallica',
  videoUrl: 'https://drive.google.com/file/d/1GXRC3jDAoJX_n6ZKG1tEOv1uDDc8Qb8s/view?usp=sharing',
})