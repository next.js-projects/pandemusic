import videoPattern from '../_default'

export default videoPattern({
  name: 'Wind Of Change',
  artist: 'Scorpions',
  videoUrl: 'https://drive.google.com/file/d/1HsBuATZF0p5u-PW1RmWp6zdBrCJD6p82/view?usp=sharing',
})