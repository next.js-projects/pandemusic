import videoPattern from '../_default'

export default videoPattern({
  name: 'Dentro de um abraço',
  artist: 'Jota Quest',
  linkUrl: 'abraco-jota-quest',
  videoUrl: 'https://drive.google.com/file/d/1Gqpct8tlFqMrdPaCfGbwvTtDDzpRak82/view?usp=sharing',
})