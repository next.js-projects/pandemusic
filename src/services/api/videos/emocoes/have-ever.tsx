import videoPattern from '../_default'

export default videoPattern({
  name: 'Have You Ever Seen The Rain',
  artist: 'Creedence',
  videoUrl: 'https://drive.google.com/file/d/1Hslk0AHb5ojXHawd6yWauyTgq8YMmq3x/view?usp=sharing',
})