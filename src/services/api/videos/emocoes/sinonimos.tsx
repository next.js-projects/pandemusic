import videoPattern from '../_default'

export default videoPattern({
  name: 'Sinónimos',
  artist: 'Zé Ramalho',
  videoUrl: 'https://drive.google.com/file/d/1JPHq1tv9ipOjnSWIPA94LKmjC624AxG-/view?usp=sharing',
})