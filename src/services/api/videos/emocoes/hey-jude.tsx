import videoPattern from '../_default'

export default videoPattern({
  name: 'Hey Jude',
  artist: 'The Beatles',
  linkUrl: 'hey-jude-the-beatles',
  videoUrl: 'https://drive.google.com/file/d/1MZgH_pHZkEiFMoKFM9vfZwTO6DT-P7AY/view?usp=sharing',
})