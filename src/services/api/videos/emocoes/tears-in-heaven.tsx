import videoPattern from '../_default'

export default videoPattern({
  name: 'Tears in heaven',
  artist: 'Erick Clapton',
  linkUrl: 'tears-in-heaven-erick-clapton',
  videoUrl: 'https://drive.google.com/file/d/1MtZA3Th6rdgF4FgUtUrm4qtS0KGwUWXh/view?usp=sharing',
})